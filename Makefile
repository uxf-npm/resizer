dev:
	UXF_RESIZER_SOURCE_PATH=${PWD}/example/source UXF_RESIZER_GENERATE_PATH=${PWD}/example/generated ./node_modules/.bin/ts-node ./bin/uxf-resizer.ts

lint:
	./node_modules/.bin/eslint -c .eslintrc.js "./**/*.ts*"

typecheck:
	./node_modules/.bin/tsc --noEmit --skipLibCheck

test: lint typecheck