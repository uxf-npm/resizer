import express from "express";
import { existsSync, mkdirSync } from "fs";
import * as fs from "fs";
import sharp, { ResizeOptions } from "sharp";

const FIT_OPTIONS: Record<string, ResizeOptions["fit"]> = {
    cv: "cover",
    f: "fill",
    cn: "contain",
    in: "inside",
    out: "outside",
} as const;

const POSITION_OPTIONS: Record<string, ResizeOptions["position"]> = {
    a: "attention",
    b: "bottom",
    c: "centre",
    e: "entropy",
    l: "left",
    lb: "left bottom",
    lt: "left top",
    r: "right",
    rb: "right bottom",
    rt: "right top",
    t: "top",
} as const;

type OutputFormat = "jpeg" | "png" | "webp" | "avif";

interface UrlParams {
    background: string;
    ext: string;
    fit: keyof typeof FIT_OPTIONS;
    toFormat: OutputFormat;
    height: string;
    namespace: string;
    position: keyof typeof POSITION_OPTIONS;
    trim: string;
    uuid: string;
    width: string;
}

const isTrim = (value: string): boolean => value !== "nt";
const getTrimValue = (value: string): number => (isTrim(value) ? Number(value) : 0);

export const createApp = (sourcePath: string, generatePath: string) =>
    express().get(
        "/generated/:namespace/:p1/:p2/:uuid[_]:width(\\d+)[_]:height(\\d+)[_]:fit[_]:position[_]:background[_]:trim[_]:ext.:toFormat",
        async (req, res) => {
            const { background, ext, fit, toFormat, height, namespace, position, trim, uuid, width } =
                req.params as unknown as UrlParams;

            const filename = `${uuid}_${width}_${height}_${fit}_${position}_${background}_${trim}_${ext}.${toFormat}`;
            const dirPath = [generatePath, namespace, uuid.charAt(0), uuid.charAt(1)].join("/");
            const filePath = `${dirPath}/${filename}`;

            if (fs.existsSync(filePath)) {
                return res.sendFile(filePath);
            }

            if (!existsSync(dirPath)) {
                mkdirSync(dirPath, { recursive: true });
            }

            const pipeline = await sharp(`${sourcePath}/${namespace}/${uuid}.${ext}`)
                .rotate()
                .resize(width ? Number(width) : undefined, height ? Number(height) : undefined, {
                    background: background === "t" ? "transparent" : `#${background}`,
                    fastShrinkOnLoad: false,
                    fit: FIT_OPTIONS[fit] || "cover",
                    position: POSITION_OPTIONS[position] || "centre",
                    withoutEnlargement: ext !== "svg",
                });

            if (isTrim(trim)) {
                pipeline.trim(getTrimValue(trim));
            }

            let contentType;

            switch (toFormat) {
                case "webp":
                    pipeline.webp({ quality: 60 });
                    contentType = "image/webp";
                    break;
                case "png":
                    pipeline.png({ progressive: true, quality: 60 });
                    contentType = "image/png";
                    break;
                case "avif":
                    pipeline.avif();
                    contentType = "image/avif";
                    break;
                default:
                    pipeline.jpeg({ progressive: true, quality: 60 });
                    contentType = "image/jpeg";
                    break;
            }

            const cacheMaxAge = 7 * 24 * 60 * 60; // week
            res.setHeader("cache-control", `public, max-age=${cacheMaxAge}`);
            res.setHeader("Content-Type", contentType);

            pipeline
                .withMetadata({ density: 72 })
                .toColorspace("srgb")
                .toFile(filePath)
                .then(() => res.sendFile(filePath))
                .catch(e => {
                    // eslint-disable-next-line no-console
                    console.log(e);
                    return res.sendStatus(404);
                });
        },
    );
