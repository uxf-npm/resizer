# @uxf/resizer

```
// install resizer
yarn global add @uxf/resizer

// run resizer
UXF_RESIZER_SOURCE=/path/to/source/directory UXF_RESIZER_GENERATED=/path/to/generated/directory uxf-resizer
```