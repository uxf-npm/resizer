#!/usr/bin/env node
import { argv, env } from "process";
import cli from "yargs";
import { createApp } from "../index";

const uxfResizer = async () => {
    cli.command("$0", "UXF Resizer", yargs => {
        yargs.demandCommand(0, 0).usage(`UXF Resizer
Usage:
  uxf-resizer
  
Environment variables:
  UXF_RESIZER_SOURCE_PATH - required
  UXF_RESIZER_GENERATE_PATH - required`);
    })
        .option("h", { alias: "help", group: "Options" })
        .strict(false)
        .exitProcess(false);

    try {
        const { help } = cli.parse(argv.slice(2));

        if (help) {
            return 0;
        }
        const sourcePath = env.UXF_RESIZER_SOURCE_PATH;
        if (!sourcePath) {
            // eslint-disable-next-line no-console
            console.log("Source directory must be set. Use environment variable UXF_RESIZER_SOURCE.");
            return 1;
        }

        const generatePath = env.UXF_RESIZER_GENERATE_PATH;
        if (!generatePath) {
            // eslint-disable-next-line no-console
            console.log("Generated directory must be set. Use environment variable UXF_RESIZER_GENERATED.");
            return 1;
        }

        return new Promise(resolve => {
            const app = createApp(sourcePath, generatePath);
            const listenedApp = app.listen(5000, () => {
                // eslint-disable-next-line no-console
                console.log(`SOURCE_PATH=${sourcePath}`);
                // eslint-disable-next-line no-console
                console.log(`GENERATED_PATH=${generatePath}`);
                // eslint-disable-next-line no-console
                console.log("Image resizer app listening on port 5000");

                if (process.env.NODE_ENV !== "production") {
                    // eslint-disable-next-line no-console
                    console.log(
                        "Example image: http://localhost:5000/generated/namespace/i/m/img_300_500_cv_c_t_nt_png.png",
                    );
                }

                app.on("close", () => {
                    listenedApp.close();
                    resolve(true);
                });
            });
        });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        return 1;
    }
};

// eslint-disable-next-line no-console
uxfResizer().catch(console.error);
